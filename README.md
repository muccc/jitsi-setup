# Experimental Jitsi Setup

Experimentelles Jitsi Setup auf Basis von Ansible.

Basiert weitestgehend auf dem upstream Jitsi installer. Configs wie z.B. nginx sind explizit kopiert und vom template installiert, um mehr kontrolle und transparenz zu ermöglichen.

Nicht geeignet für ein langfristiges deployment.

## Pirvacy-Features
* Source natting aller incoming requests (v4 und v6)

## Ausführen

Das playbook kann z.B. folgendermaßen für einen einzelnen Host ausgeführt werden

```
$ ansible-playbook -i inventory playbook.yml --limit 88.198.44.130 -K -b
```

`-K -b` sorgen dafür, dass ansible per sudo Privilegien eskaliert.

Es kann leider dazu kommen, dass generell, insbesondere beim initialen Setup eine Race-Condition zwischen nginx und jitsi-videobridge startup passiert, in der jitsi-videobridge Port 80 und 443 belegt. In dem Fall jitsi-videobridge stoppen, nginx neustarten und dann jitsi-videobridge wieder starten. Gïbt bestimmt ne config um das zu ändern.
